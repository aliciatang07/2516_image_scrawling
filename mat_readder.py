import scipy.io as sio
import json 

mat = sio.loadmat('cars_annos.mat')
# print(mat)
annotation = mat["annotations"].tolist()
car_label = {}
car_distribution = {}
for i in range(len(annotation[0])):
    label_index = annotation[0][i][5][0][0]
    label = mat["class_names"][0][label_index-1][0]
    car_label[annotation[0][i][0][0]] = label

for j in range(1,len(annotation[0])):
    prev_label_index = annotation[0][j-1][5][0][0]
    prev_label = mat["class_names"][0][prev_label_index-1][0]
    cur_label_index = annotation[0][j][5][0][0]
    cur_label = mat["class_names"][0][cur_label_index-1][0]
    if(prev_label_index == cur_label_index):     
        car_distribution[prev_label] =  car_distribution.get(prev_label,1)+1
    else:
        car_distribution[prev_label] =  car_distribution.get(prev_label,1)
        car_distribution[cur_label] =  1


json_label = json.dumps(car_label)
json_distribution = json.dumps(car_distribution)
with open("car_label.json","w") as f:
    f.write(json_label)
with open("label_distribution.json","w") as f:
    f.write(json_distribution)
# list = sio.whosmat('cars_annos.mat')

