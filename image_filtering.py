import sys
import os 
import PIL
from PIL import Image


if __name__ == "__main__":
    data_dir = sys.argv[1]

    for file in os.listdir(data_dir):
        if file.endswith(".jpg"):
            try:
                img = Image.open(data_dir+file)
            except Exception as e:
                print("file {} meet errors".format(file))
                os.remove(data_dir+file)

