car_label.json and label_distribution.json are already provided.
(To generate new label_distribution json file, run 
``` python mat_readder.py``` with cars_annotations.mat in the same folder 
you will get car_label.json and label_distribution.json )

With these two json files, you can get run 
``` python image_scrappler.py ``` get all car image in data folder. (it takes around 4hours to download 9956 images)
And then conduct image filtering by running
```python image_filtering.py data folder``` to filter out cropped image 

