# Reference from https://towardsdatascience.com/image-scraping-with-python-a96feda8af2d

from email.mime import image
import time 
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
import urllib.request
import json 
import drive_sample
import PIL
from PIL import Image
import os 

def fetch_image_urls(query:str, max_links_to_fetch:int, wd:webdriver, sleep_between_interactions:int=1):
    def scroll_to_end(wd):
        wd.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(sleep_between_interactions)    
    
    # build the google query
    search_url = "https://www.google.com/search?safe=off&site=&tbm=isch&source=hp&q={q}&oq={q}&gs_l=img"
    # load the page
    wd.get(search_url.format(q=query))

    image_urls = set()
    image_count = 0
    results_start = 0
    while image_count < max_links_to_fetch:
        scroll_to_end(wd)

        # get all image thumbnail results
        thumbnail_results = wd.find_elements_by_css_selector("img.Q4LuWd")
        number_results = len(thumbnail_results)
        if(number_results>max_links_to_fetch):
            if(results_start == 0):
                number_results = max_links_to_fetch
            else:
                number_results = results_start+max_links_to_fetch-image_count
        print(f"Found: {number_results} search results. Extracting links from {results_start}:{number_results}")
        
        for img in thumbnail_results[results_start:number_results]:
            # try to click every thumbnail such that we can get the real image behind it
            try:
                img.click()
                time.sleep(sleep_between_interactions)
            except Exception:
                continue

            # extract image urls    
            actual_images = wd.find_elements_by_css_selector('img.n3VNCb')
            for actual_image in actual_images:
                if actual_image.get_attribute('src') and 'http' in actual_image.get_attribute('src'):
                    image_urls.add(actual_image.get_attribute('src'))

            image_count = len(image_urls)

        if len(image_urls) >= max_links_to_fetch:
            print(f"Found: {len(image_urls)} image links, done!")
            break
        else:
            print("Found:", len(image_urls), "image links, looking for more ...")
            time.sleep(1)
            load_more_button = wd.find_element_by_css_selector(".mye4qd")
            if load_more_button:
                wd.execute_script("document.querySelector('.mye4qd').click();")

        # move the result startpoint further down
        results_start = len(thumbnail_results)
    wd.quit()
    print(image_urls)
    return image_urls


def download_images(key,urls):
    path = "./data"

    if not os.path.exists(path):
        os.makedirs(path)
        
    lens = len(urls)
    for i in range(lens):
        try:
            image = urllib.request.urlopen(urls[i]).read()
        except Exception as e:
            print("image {} is corrupted due to {}".format(i, e))
            continue
        try:
            file_name = path+key+"_"+str(i)+".jpg"
            with open(file_name,"wb") as f:
                f.write(urllib.request.urlopen(urls[i]).read())
                f.close()
            img = Image.open(file_name)
          
            img = img.resize((224,224))
            img.save(file_name) 
        except Exception as e:
            print(e)

def load_dict():
    with open('label_distribution.json','r') as f:
        data = json.load(f)
        return data


if __name__ == "__main__":

    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox') # required when running as root user. otherwise you would get no sandbox errors. 
    # driver = webdriver.Chrome(ChromeDriverManager().install(),chrome_options=chrome_options)


    dicts = load_dict()
    keys = dicts.keys()
    # # print(keys)
    for key in keys:
        driver = webdriver.Chrome(ChromeDriverManager().install(),chrome_options=chrome_options)
        urls = list(fetch_image_urls(key,int(dicts[key]/2),driver))
        download_images(key,urls)
        time.sleep(5)
